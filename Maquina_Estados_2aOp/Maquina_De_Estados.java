import java.io.CharArrayWriter;
import java.io.CharArrayReader;
import java.io.CharConversionException;
import java.util.Scanner;

public class Maquina_De_Estados{

    int contador;
    static char [] caracteres;
    

    public static void main(String[] args) {
        FiniteStateMachine mde=new FiniteStateMachine();
        Scanner sc = new Scanner(System.in);
        int c = sc.nextInt();
        char[] caracteres = c.toCharArray();
        mde.inicio();
    }
    public void inicio(){
        contador=0;
        Go_Norte();
        Wait_Norte();
        Go_Este();
        Wait_Este();
    }

    public void Go_Norte(){
        System.out.println("Estado Go_Norte "+ c);
        if(contador<caracteres.length){
            if(caracteres[contador]==00){
                contador++;
                Go_Norte();
            }
            else if(caracteres[contador]==01){
                contador++;
                Wait_Norte();
            }
        }
    }

    public void Wait_Norte(){
        System.out.println("Estado Wait_Norte "+c);
        if(contador<caracteres.length){
            if(caracteres[contador]==01){
                contador++;
                Wait_Norte();
            }
            else if(caracteres[contador]==000111){
                contador++;
                Go_Este();
            }
        }
    }

    public void Go_Este(){
        System.out.println("Estado Go_Este "+c);
        if(contador<caracteres.length){
            if(caracteres[contador]==0001){
                contador++;
                Go_Este();
            }
            else if(caracteres[contador]==1011){
                contador++;
                Wait_Este();
            }
        }
    }

    public void Wait_Este(){
        System.out.println("Estado Wait_Este "+c);
        if(contador<caracteres.length){
            if(caracteres[contador]==1011){
                contador++;
                Wait_Este();
            }
            else if(caracteres[contador]==00011011){
                contador++;
                Go_Norte();
            }
        }

    }

}